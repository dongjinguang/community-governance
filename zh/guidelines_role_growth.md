# **OpenHarmony社区角色定义及晋升机制**

## 1. 角色定义

| 序号 | 角色           | 职责                                                                                                                                                                                                                                                                                                                                                                                                                           |
| :--- | :------------- | :----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| 1    | PMC成员（PMC） | 项目管理委员会（PMC）成员，拥有代码库写权限、OpenHarmony 新版本发布、Roadmap发布、新PMC/Committer等社区事务的投票权、以及新的 PMC 成员和 Committer 提名权。PMC负责OpenHarmony 社区的管理工作，包括开源OpenHarmony 社区版本规划、竞争力规划、特性开发代码维护、资料开发、补丁规划等；组织PMC委员的选举和退出，负责Committer的任命和退出；负责OpenHarmony 社区SIG的申请准入、SIG孵化项目指导、SIG毕业项目准入等SIG生命周期管理等 |
| 2    | SIG组长（SIG Leader） | SIG Leader负责特定SIG的运营及维护。SIG Leader负责定义特定SIG的工作范围及业务目标，并负责对应SIG的运营及维护；吸纳并发展Committer参与对应SIG的项目孵化、文档完善及社区推广；定期在PMC项目管理委员会汇报SIG孵化项目及SIG运营进展，并基于PMC的指导建议完成相关改进。 |
| 3    | 审核者（Committer） | Committer拥有SIG子领域的代码仓写权限。Committer 负责SIG领域软件模块设计与评审，负责代码审核及维护，处理OpenHarmony社区的issue、邮件列表问题，辅导Contributors快速理解SIG领域架构设计并提升代码开发技能 |
| 4    | 评审员（Reviewer） | Reviewer代码仓评审权限，可以主动出发CI门禁和分配责任人，参与OpenHarmony 社区代码贡献、文档贡献、技术方案的贡献和预审工作 |
| 5    | 贡献者（Contributor） | 有一定代码编程经验的开发者。Contributors以参与OpenHarmony 社区代码贡献、文档贡献、技术方案讨论及设计、解答用户问题、发表技术文章及视频课程、组织策划开源OpenHarmony 社区活动等形式参与OpenHarmony 社区。 |
| 6    | 用户 (Users) | 使用OpenHarmony项目的广大用户，以Issue形式向OpenHarmony 社区反馈问题和功能建议。 |

## 2. 晋升标准和流程

### 2.1 晋升标准简介:

- 2.1.1 Reviewer任职标准:
      - 1. OpenHarmony项目贡献至少2个月
      - 2. 至少有5个PR的有效审阅意见
      - 3. 至少有10个实质性的代码PR提交
      - 4. 熟悉所申请的仓库的代码架构
      - 5. 由对应仓库的Committer或SIG Leader提名，并维护到对应仓库的CODEOWNERS文件中

- 2.1.2 Committer任职标准:
      - 1. OpenHarmony项目贡献至少3个月
      - 2. 至少10个PR的有效审阅意见
      - 3. 至少有30个实质性的代码PR提交
      - 4. 非常熟悉所申请的仓库的代码架构
      - 5. 由对应仓库的Committer或SIG Leader或PMC成员提名，经PMC投票通过后，维护到committer列表文件中

- 2.1.3 SIG Leader任职标准:
      - 1. SIG发起人经过PMC团队审核通过后，可以默认成为对应领域SIG Leader
      - 2. 需要先具备对应SIG领域Committer角色，并且对SIG技术领域全景非常熟悉
      - 3. 由对应SIG Leader或PMC成员提名，经PMC会议评审通过后，维护到对应SIG领域owner文件中

- 2.1.4 PMC任职标准:
      - 1. OpenHarmony技术领域SIG突出贡献个人或团队
      - 2. 对OpenHarmony技术框架、演进方向、开源项目治理深入理解的突出贡献Committer或SIG Leader
      - 3. 由对应SIG Leader或PMC成员提名，经PMC会议评审通过后，维护到对应PMC列表文件中

### 2.2 晋升流程

- 2.2.1 晋升Reviewer流程:
      - 1. 由对应仓库的Committer或SIG Leader提名申请
      - 2. 在领域SIG例会评审通过后，即可成为对应仓库的Reviewer
      - 3. 由对应仓库Committer根据SIG例会评审意见，对应仓库的CODEOWNERS文件中

- 2.2.2 晋升Committer流程:
      - 1. 由对应领域SIG Leader/Committer提名，以标题“[VOTE] New Committer for xxx ”发送邮件至[pmc@openharmony.io](mailto:pmc@openharmony.io)。
      - 2. 所有PMC成员有权通过“+1”或“-1”形式表示支持或反对，PMC通过回复邮件发送投票结果，投票时间一般持续72个小时。
      - 3. 提名Committer获得三票及以上赞成票，无反对票情况下投票通过。投反对票的PMC成员须说明反对的具体原因（无原因描述的反对票无效），投票发起人可针对具体问题进行澄清。
      - 4. 投票通过后，PMC成员在OpenHarmony dev和pmc邮件列表中进行公告新Committer。
      - 5. 新Committer 收到公示邮件后，按照要求格式发送对应信息至[pmc@openharmony.io](mailto:pmc@openharmony.io)。
      - 6. 新Committer 将个人信息提交PR到 [committer维护清单](./committer.md) 。

- 2.2.3 SIG Leader变更流程:
      - 1. 由对应领域SIG Leader提名议题申请到PMC理会进行评审决策。
      - 2. 评审通过后，由对应领域责任人提交PR至对应[sig维护清单](../sig/sig-xxx)进行更新。

- 2.2.4 晋升PMC流程:
      - 1. 由现任PMC提名，以标题“[VOTE] New PMC xxx ”发送邮件至[pmc@openharmony.io](mailto:pmc@openharmony.io)。
      - 2. 所有PMC成员有权通过“+1”或“-1”形式表示支持或反对，PMC通过回复邮件发送投票结果。投票时间一般持续72个小时。
      - 3. 提名PMC获得2/3以上赞成票，无反对票情况下投票通过。投反对票的PMC成员须说明反对的具体原因（无原因描述的反对票无效），投票发起人可针对具体问题进行澄清。
      - 4. 投票通过后，PMC成员在OpenHarmony dev和pmc邮件列表中进行公告新PMC。
      - 5. 新PMC 收到公示邮件后，按照要求格式发送对应信息至[pmc@openharmony.io](mailto:pmc@openharmony.io)。
      - 6. 新PMC 将个人信息提交PR到 [PMC维护清单](./pmc)


## 3. 非活跃成员退出标准和流程

### 3.1 非活跃成员定义:

- 3.1.1 非活跃Committer: 连续3个月以上没有在OpenHarmony 社区参与代码审核、PR提交、设计评审等
- 3.1.2 非活跃SIG： 连续3个月以上未召开SIG小组会议，连续6个月以上没有在PMC项目管理委员会汇报SIG孵化项目及SIG运营进展
- 3.1.3 非活跃PMC成员： 连续3个月以上未参与PMC会议，连续6个月以上没有在OpenHarmony 社区参与代码审核、PR提交、设计评审、社区活动推广等

### 3.2 非活跃成员退出流程:

- 3.2.1  维持健康社区的核心原则是鼓励大家积极参与，由于Committer、SIG Leader、PMC的自身工作或职责的调整，不可避免地会无法积极地进行社区贡献。如果您知道您将在很长一段时间内无法参与社区贡献，您应该通知PMC，我们会将您标记为“不活跃”。不活跃的Committer、SIG Leader、PMC将从 OpenHarmony 的项目清单中删除，并且不再具有响应的权利；您在OpenHarmony获得的成绩永不过期，一旦您再次活跃，将被要求通过PMC申请，按照推举流程重新获得对应的资格。

- 3.2.2 由您自己发邮件申请退出OpenHarmony项目Committer、SIG Leader、PMC成员，以标题“Inactive [Committer/SIG Leader/PMC] xxx ” 发送邮件至[pmc@openharmony.io](mailto:pmc@openharmony.io)；

- 3.2.3 按季度在PMC例会审视，对不活跃Committer、SIG Leader、PMC成员进行退出处理，并在对应配置文件中进行公示；
